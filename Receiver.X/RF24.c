#include "RF24.h"

uint8_t read(uint8_t reg)
{
	CSN_SetLow();
	SPI_Exchange8bit(R_REGISTER | (REGISTER_MASK & reg));
	uint8_t result = SPI_Exchange8bit(RF_NOP);
	CSN_SetHigh();
	return result;
}

uint8_t write(uint8_t reg, uint8_t value)
{
	CSN_SetLow();
	uint8_t result = SPI_Exchange8bit(W_REGISTER | (REGISTER_MASK & reg));
	SPI_Exchange8bit(value);
	CSN_SetHigh();
	return result;
}

uint8_t write_buffer(uint8_t reg, const uint8_t* buffer, uint8_t len)
{
	CSN_SetLow();
  uint8_t result = SPI_Exchange8bit(W_REGISTER | (REGISTER_MASK & reg));
  while (len--)
    SPI_Exchange8bit(*buffer++);
	CSN_SetHigh();
  return result;
}

uint8_t clear_status(void)
{
	return write(STATUS,(1 << RX_DR) | (1 << TX_DS) | (1 << MAX_RT));
}

void rf_init_receiver(uint8_t* address, uint8_t payload_size)
{
	CE_SetLow();
	CSN_SetHigh();
  __delay_ms(5) ;
  write(CONFIG,0b00111011);
	write(RX_PW_P1,payload_size);
  write_buffer(RX_ADDR_P1,address,5);
	clear_status();
  CE_SetHigh();
}

bool rf_received_payload(void)
{
	return !(read(FIFO_STATUS) & (1 << RX_EMPTY));
}

void rf_read_payload(uint8_t* buffer, uint8_t len)
{
	CSN_SetLow();
  SPI_Exchange8bit(R_RX_PAYLOAD);
  while (len--)
    *buffer++ = SPI_Exchange8bit(RF_NOP);
	CSN_SetHigh();
	clear_status();
}
