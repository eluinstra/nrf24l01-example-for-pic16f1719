/* 
 * File:   RF24.h
 * Author: edwin
 *
 * Created on March 6, 2016, 9:13 PM
 */

#ifndef RF24_H
#define	RF24_H

#ifdef	__cplusplus
extern "C"
{
#endif

#include "mcc_generated_files/mcc.h"
#include "nRF24L01.h"

void rf_init_receiver(uint8_t* address, uint8_t payload_size);
bool rf_received_payload(void);
void rf_read_payload(uint8_t* buffer, uint8_t len);

#ifdef	__cplusplus
}
#endif

#endif	/* RF24_H */

