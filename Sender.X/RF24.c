#include "RF24.h"

uint8_t read(uint8_t reg)
{
	CSN_SetLow();
	SPI_Exchange8bit(R_REGISTER | (REGISTER_MASK & reg));
	uint8_t result = SPI_Exchange8bit(RF_NOP);
	CSN_SetHigh();
	return result;
}

uint8_t write_cmd(uint8_t cmd)
{
	CSN_SetLow();
  uint8_t result = SPI_Exchange8bit(cmd);
	CSN_SetHigh();
  return result;
}

uint8_t write(uint8_t reg, uint8_t value)
{
	CSN_SetLow();
	uint8_t result = SPI_Exchange8bit(W_REGISTER | (REGISTER_MASK & reg));
	SPI_Exchange8bit(value);
	CSN_SetHigh();
	return result;
}

uint8_t write_buffer(uint8_t reg, const uint8_t* buffer, uint8_t len)
{
	CSN_SetLow();
  uint8_t result = SPI_Exchange8bit(W_REGISTER | (REGISTER_MASK & reg));
  while (len--)
    SPI_Exchange8bit(*buffer++);
	CSN_SetHigh();
  return result;
}

uint8_t clear_status(void)
{
	return write(STATUS,(1 << RX_DR) | (1 << TX_DS) | (1 << MAX_RT));
}

uint8_t get_status(void)
{
  return write_cmd(RF_NOP);
}

uint8_t flush_tx(void)
{
  return write_cmd(FLUSH_TX);
}

void rf_init_sender(uint8_t* address)
{
	CE_SetLow();
	CSN_SetHigh();
	__delay_ms(5);
	write(CONFIG,0b01001010);
	write_buffer(RX_ADDR_P0,address,5);
	write_buffer(TX_ADDR,address,5);
	clear_status();
}

bool rf_write_payload(const uint8_t* buffer, uint8_t len)
{
	CSN_SetLow();
  SPI_Exchange8bit(W_TX_PAYLOAD);
  while (len--)
    SPI_Exchange8bit(*buffer++);
	CSN_SetHigh();
	CE_SetHigh();
	while (!(get_status() & ((1 << TX_DS) | (1 << MAX_RT))));
	CE_SetLow();
  if(clear_status() & (1 << MAX_RT))
	{
  	flush_tx();
  	return 0;
  }
  return 1;
}