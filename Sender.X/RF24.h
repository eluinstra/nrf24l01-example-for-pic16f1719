/* 
 * File:   RF24.h
 * Author: eluinstra
 *
 * Created on March 6, 2016, 1:59 PM
 */

#ifndef RF24_H
#define	RF24_H

#ifdef	__cplusplus
extern "C"
{
#endif

#include "mcc_generated_files/mcc.h"
#include "nRF24L01.h"

void rf_init_sender(uint8_t* address);
bool rf_write_payload(const uint8_t* buffer, uint8_t len);

#ifdef	__cplusplus
}
#endif

#endif	/* RF24_H */

