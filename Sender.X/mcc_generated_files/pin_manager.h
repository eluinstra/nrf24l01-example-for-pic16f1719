/**
	@Generated Pin Manager Header File

	@Company:
		Microchip Technology Inc.

	@File Name:
		pin_manager.h

	@Summary:
		This is the Pin Manager file generated using MPLAB� Code Configurator

	@Description:
		This header file provides implementations for pin APIs for all pins selected in the GUI.
		Generation Information :
				Product Revision  :  MPLAB� Code Configurator - v2.25.2
				Device            :  PIC16F1719
				Version           :  1.01
		The generated drivers are tested against the following:
				Compiler          :  XC8 v1.34
				MPLAB             :  MPLAB X v2.35 or v3.00
 */

/*
Copyright (c) 2013 - 2015 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 */

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set CE aliases
#define CE_TRIS               TRISC1
#define CE_LAT                LATC1
#define CE_PORT               RC1
#define CE_WPU                WPUC1
#define CE_SetHigh()    do { LATC1 = 1; } while(0)
#define CE_SetLow()   do { LATC1 = 0; } while(0)
#define CE_Toggle()   do { LATC1 = ~LATC1; } while(0)
#define CE_GetValue()         RC1
#define CE_SetDigitalInput()    do { TRISC1 = 1; } while(0)
#define CE_SetDigitalOutput()   do { TRISC1 = 0; } while(0)

#define CE_SetPullup()    do { WPUC1 = 1; } while(0)
#define CE_ResetPullup()   do { WPUC1 = 0; } while(0)
// get/set CSN aliases
#define CSN_TRIS               TRISC2
#define CSN_LAT                LATC2
#define CSN_PORT               RC2
#define CSN_WPU                WPUC2
#define CSN_ANS                ANSC2
#define CSN_SetHigh()    do { LATC2 = 1; } while(0)
#define CSN_SetLow()   do { LATC2 = 0; } while(0)
#define CSN_Toggle()   do { LATC2 = ~LATC2; } while(0)
#define CSN_GetValue()         RC2
#define CSN_SetDigitalInput()    do { TRISC2 = 1; } while(0)
#define CSN_SetDigitalOutput()   do { TRISC2 = 0; } while(0)

#define CSN_SetPullup()    do { WPUC2 = 1; } while(0)
#define CSN_ResetPullup()   do { WPUC2 = 0; } while(0)
#define CSN_SetAnalogMode()   do { ANSC2 = 1; } while(0)
#define CSN_SetDigitalMode()   do { ANSC2 = 0; } while(0)
// get/set SCK aliases
#define SCK_TRIS               TRISC3
#define SCK_LAT                LATC3
#define SCK_PORT               RC3
#define SCK_WPU                WPUC3
#define SCK_ANS                ANSC3
#define SCK_SetHigh()    do { LATC3 = 1; } while(0)
#define SCK_SetLow()   do { LATC3 = 0; } while(0)
#define SCK_Toggle()   do { LATC3 = ~LATC3; } while(0)
#define SCK_GetValue()         RC3
#define SCK_SetDigitalInput()    do { TRISC3 = 1; } while(0)
#define SCK_SetDigitalOutput()   do { TRISC3 = 0; } while(0)

#define SCK_SetPullup()    do { WPUC3 = 1; } while(0)
#define SCK_ResetPullup()   do { WPUC3 = 0; } while(0)
#define SCK_SetAnalogMode()   do { ANSC3 = 1; } while(0)
#define SCK_SetDigitalMode()   do { ANSC3 = 0; } while(0)
// get/set SDI aliases
#define SDI_TRIS               TRISC4
#define SDI_LAT                LATC4
#define SDI_PORT               RC4
#define SDI_WPU                WPUC4
#define SDI_ANS                ANSC4
#define SDI_SetHigh()    do { LATC4 = 1; } while(0)
#define SDI_SetLow()   do { LATC4 = 0; } while(0)
#define SDI_Toggle()   do { LATC4 = ~LATC4; } while(0)
#define SDI_GetValue()         RC4
#define SDI_SetDigitalInput()    do { TRISC4 = 1; } while(0)
#define SDI_SetDigitalOutput()   do { TRISC4 = 0; } while(0)

#define SDI_SetPullup()    do { WPUC4 = 1; } while(0)
#define SDI_ResetPullup()   do { WPUC4 = 0; } while(0)
#define SDI_SetAnalogMode()   do { ANSC4 = 1; } while(0)
#define SDI_SetDigitalMode()   do { ANSC4 = 0; } while(0)
// get/set SDO aliases
#define SDO_TRIS               TRISC5
#define SDO_LAT                LATC5
#define SDO_PORT               RC5
#define SDO_WPU                WPUC5
#define SDO_ANS                ANSC5
#define SDO_SetHigh()    do { LATC5 = 1; } while(0)
#define SDO_SetLow()   do { LATC5 = 0; } while(0)
#define SDO_Toggle()   do { LATC5 = ~LATC5; } while(0)
#define SDO_GetValue()         RC5
#define SDO_SetDigitalInput()    do { TRISC5 = 1; } while(0)
#define SDO_SetDigitalOutput()   do { TRISC5 = 0; } while(0)

#define SDO_SetPullup()    do { WPUC5 = 1; } while(0)
#define SDO_ResetPullup()   do { WPUC5 = 0; } while(0)
#define SDO_SetAnalogMode()   do { ANSC5 = 1; } while(0)
#define SDO_SetDigitalMode()   do { ANSC5 = 0; } while(0)
// get/set TX aliases
#define TX_TRIS               TRISC6
#define TX_LAT                LATC6
#define TX_PORT               RC6
#define TX_WPU                WPUC6
#define TX_ANS                ANSC6
#define TX_SetHigh()    do { LATC6 = 1; } while(0)
#define TX_SetLow()   do { LATC6 = 0; } while(0)
#define TX_Toggle()   do { LATC6 = ~LATC6; } while(0)
#define TX_GetValue()         RC6
#define TX_SetDigitalInput()    do { TRISC6 = 1; } while(0)
#define TX_SetDigitalOutput()   do { TRISC6 = 0; } while(0)

#define TX_SetPullup()    do { WPUC6 = 1; } while(0)
#define TX_ResetPullup()   do { WPUC6 = 0; } while(0)
#define TX_SetAnalogMode()   do { ANSC6 = 1; } while(0)
#define TX_SetDigitalMode()   do { ANSC6 = 0; } while(0)
// get/set RX aliases
#define RX_TRIS               TRISC7
#define RX_LAT                LATC7
#define RX_PORT               RC7
#define RX_WPU                WPUC7
#define RX_ANS                ANSC7
#define RX_SetHigh()    do { LATC7 = 1; } while(0)
#define RX_SetLow()   do { LATC7 = 0; } while(0)
#define RX_Toggle()   do { LATC7 = ~LATC7; } while(0)
#define RX_GetValue()         RC7
#define RX_SetDigitalInput()    do { TRISC7 = 1; } while(0)
#define RX_SetDigitalOutput()   do { TRISC7 = 0; } while(0)

#define RX_SetPullup()    do { WPUC7 = 1; } while(0)
#define RX_ResetPullup()   do { WPUC7 = 0; } while(0)
#define RX_SetAnalogMode()   do { ANSC7 = 1; } while(0)
#define RX_SetDigitalMode()   do { ANSC7 = 0; } while(0)

/**
 * @Param
		none
 * @Returns
		none
 * @Description
		GPIO and peripheral I/O initialization
 * @Example
		PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize(void);

/**
 * @Param
		none
 * @Returns
		none
 * @Description
		Interrupt on Change Handling routine
 * @Example
		PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);

#endif // PIN_MANAGER_H
/**
 End of File
 */