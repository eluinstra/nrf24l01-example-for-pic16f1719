/* 
 * File:   RF24.h
 * Author: eluinstra
 *
 * Created on March 6, 2016, 10:44 PM
 */

#ifndef RF24_H
#define	RF24_H

#ifdef	__cplusplus
extern "C"
{
#endif

#include "mcc_generated_files/mcc.h"
#include "nRF24L01.h"

const uint8_t payload_size = 1;
char payload[payload_size + 1];

void rf_init_receiver(uint8_t* address, uint8_t payload_size);
uint8_t rf_clear_status(void);
bool rf_received_payload(void);
void rf_read_payload(uint8_t* buffer, uint8_t len);

#ifdef	__cplusplus
}
#endif

#endif	/* RF24_H */

